// Simile a:
// import path from "path";
//
// (require path -->importare tutti gli elementi che sono declinati come 
// esportati all'interno di percorso che andate 
const path = require("path");

// Serve ad esportare ,per lo standard require / module. exports,
// dei valori / metodi
module.exports = {
    devtool: 'eval-source-map',
    // il percorso al fine file iniziale della mia applicazione 
    // ./ significa -> segui il percorso a partire dalla cartella 
    //                 in cui si trova questo file(webpack.config.js);
    // Src è il nome che viene universalmente riconosciuto
    // per la cartella che contiene i sorgenti dell'applicato  
    entry: "./src/index.js",
    // Mode imposta la modalità di lavoro di webpack affinchè
    // vengano prodotti, qualora necessario,
    // determinati tipi di file (ad es. i file di map)
    mode:"development",
    module: {
        rules: [{
            exclude: /(node_modules|bower_components)/,
            // Webpack elabora i loader dall'ultima
            use: [{
                loader: "babel-loader"
            }, {
                loader: "prettier-loader"
            }],
            // espressione regolare per cercare 
            // x => estensione react.
            test: /\ js?$/
        }]
    },

// Definisci le informazioni relative alla destinazione del file 
// che verrà generato da webpack
    output: {

// dist è il nome universlamente riconosciuto per la cartella 
// di distribuzione, che contiene il codice da eseguire nell'ambi
// di produzione 
//
// Il contenuto di questa cartella NON deve essere mai modifica poichè
// viene generato automaticamente da webpack
        path: path.resolve(__dirname, "dist"),

// Definisce il nome del file all'interno del quale webpack 
// inserirà il codice dell'applicazione 
// Il file sarà posizionato all'interno della cartella specificata
// alla chiave path
        filename: "bundle.js",
    },
};

// A destra troviamo i telecomandi a seguire. Le seguibile, viene individuato 
// automaticamente all'interno della cartella node_modules/.bin