import { User } from "../models/user";

export function getUsers(cb) {
    var xhr = new XMLHttpRequest();

    xhr.open("GET", "https://jsonplaceholder.typicode.com/users");

    xhr.addEventListener("loadend", function() {
        var data = JSON.parse(xhr.response);
        
        // var users = data.map(function (u) {
        //     return new User(u.id, u.email, u.username);
        // });

        // Esempio con Arrow Function:
        // Passo a map una funzione anonima che ha un solo parametro formale 
        // e che restituisce (in assenza di parentesi graffe lo fa in modo
        // implicito) l'istanza creata con il costruttore User
        const users = data.map(u => new User(u.id, u.email, u.username));
        cb(users);
    });

    xhr.send();
}
