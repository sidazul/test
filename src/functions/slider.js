import { Movie } from "../models/movie";

// export function getMovies(cb, s) {
//   var xhr = new XMLHttpRequest();
//   xhr.open("GET", "https://fake-movie-database-api.herokuapp.com/api?s=" + s);

//   xhr.addEventListener("loadend", function () {
//     var data = JSON.parse(xhr.response);

//     var movie = data.Search.map(function (m) {
//       return new Movie(m.imdbID, m.Title, m.Poster);
//     });

//     cb(movie);
//   });

//   xhr.send();
// }

export function getMovies(movies, cb) {
fetch("https://fake-movie-database-api.herokuapp.com/api?s=${movie}")
  .then((res) => res.json())
  .then((data) => {
    const movies = data.Search.map(
      (m) => new Movie(m.imdID, m.Title, m. Poster)
    );

    cb(movies);
  });
}


export function sliceMovies(movies, from, to) {
  var idx = from;
  var goOn = true;
  var ret = [];

  do {
    ret.push(movies[idx]);
    
    if (idx === to) {
      goOn = false;
    } else if (idx < to || (idx > to && idx < movies.length - 1)) {
      idx++;
    } else {
      idx = 0;
    }
  } while (goOn);

  return ret;
}



