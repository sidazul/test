// entry point
import "bootstrap/dist/js/bootstrap";
// import { getUsers } from "./functions/users";
import { getMovies, sliceMovies } from "./functions/slider";
// import { User } from "./models/user";


// let users = [
//   new User ("1", "user1@gmail.com", "one"),
//   new User ("2", "user2@gmail.com", "two"),
//   new User ("3", "user3@gmail.com", "three"),
//   new User ("4", "user4@gmail.com", "four"),
//   new User ("5", "user5@gmail.com", "five"),
// ];

function loadMovies(movie, resetIndexes) {
  if (resetIndexes) {
    from = 0;
    to = 3;
  }

  slider.innerHTML = "";



  getMovies(function (apiMovies) {
    movies = apiMovies;
    // sliceMovies(movies, from, to).forEach(function (movie) {
    //   slider.innerHTML +=
    //   '<div class="col-lg-3"><img class="poster" src="' +
    //   movie.image +
    //   '"></div>';
    // });
    const slices = sliceMovies(movies, from, to);

    for (const slice of slices) {
      slider.innerHTML += `
        <div class="col-lg-3"> 
        ${slice.getPosterTag()} 
        </div>
        `;
    }
  }); 
}

// getUsers(function(users) {
//     users.forEach(function(u) {
//         tab.innerHTML += `
//             <tr>
//                 <td>${u.id}</td>
//                 <td>${u.email}</td>
//                 <td>${u.name}</td>
//                 <td>
//                   <button class="delete">Delete</button>
//                 </td>
//             </tr>
//         `;
//     });

// let buttons = document.querySelectorAll("button.delete");
//   buttons.forEach(function(b) {
//     b.addEventListener('click', function(e) {
//       e.target.parentElement.parentElement.remove();
//     });
//   });
// });



let slider = document.getElementById("slider");
let from = 0;
let to = 3;
let movies = [];

function handleSliderChange(isNext, isBack) {
  if (isNext) {
    // ++ è un operatore (restituisce un risultato)che esegue un incremento.
    //quindi se from : 0, con from++ mi dà il valore PRIMA che viene incrementato nello stack
    //Percui ++from mi dà lo STESSO valore che assume nello stack.
    from = from === movies.length - 1 ? 0 : ++from;
    to = to === movies.length - 1 ? 0 : ++to;
  } else if (isBack) {
    from = from === 0 ? movies.length - 1 : --from;
    to = to === 0 ? movies.length - 1 : --to;
  }

  slider.innerHTML = "";
  sliceMovies(movies, from, to).forEach(function (movie) {
    slider.innerHTML += `
      <div class="col-lg-3"> 
      ${movie.getPoster} 
      </div>
      `;
  });
}

document.querySelectorAll("button.slider-action").forEach(function (b) {
  b.addEventListener("click", function (e) {
    handleSliderChange(e.target.id === "slider-next",e.target.id === "slider-back");
  });
});

document.addEventListener("keydown", function(e) {
  handleSliderChange(e.key === "ArrowRight", e.key === "ArrowLeft");
});

let sel = document.querySelector('select[name="film"]');

loadMovies("batman");

sel.addEventListener("change", function (e) {
  loadMovies(e.target.value, true);
});