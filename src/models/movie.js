export class Movie {
  constructor(id, name, image) {
    this.id = id;
    this.name = name;
    this.image = image;
  }

  // getYear() {
  //   console.log("na cosa a caso", this.id);
  // }
  getPosterTag() {
  // return '<img src="' + this.image + '">';
  // return '<img src="' .concat(this.image,  '">');

  // Backtick (template literal)
  // Permette di interpolare dei valori all'interno della stringa
  // -> Il valore da interpolare deve essere declinato
  //    all'interno di ${}
  // -> Nelle tastiere US Intl. il carattere backtick si trova
  //    subito sotto ad ESC
  return '<img src="${this.image}">';
  }
}

// export function Movie(id, name, image) {
//   this.id = id;
//   this.name = name;
//   this.image = image;
// }
